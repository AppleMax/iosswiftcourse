//
//  CoreDataStack.swift
//  iosswiftcourse
//
//  Created by Maxim on 06.02.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import CoreData

class CoreDataStack {
    
    public static let shared = CoreDataStack()
    var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "iosswiftcourse")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                let nserror = error as NSError
                print("Error code: \(nserror.code)")
                print("Error domain: \(nserror.domain)")
                print("\(nserror.localizedDescription)")
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                let nserror = error as NSError
                print("Error code: \(nserror.code)")
                print("Error domain: \(nserror.domain)")
                print("\(nserror.localizedDescription)")
            }
        }
    }
}
