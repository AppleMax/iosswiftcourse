//
//  ViewController.swift
//  iosswiftcourse
//
//  Created by Maxim on 05.02.2019.
//  Copyright © 2019 Maxim Abakumov. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    var moc: NSManagedObjectContext {
        return CoreDataStack.shared.context
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /// функция createPerson создает сущность Person из разных UI-источников, как правило, это элементы UI, за исключением технического атрибута ID
    ///
    /// - Parameter context: принимает параметр текущего контекста NSManagedObjectContext Core Data
    /// - Returns: возвращает сущность Person: NSManagedObject
    func createPerson(in context: NSManagedObjectContext) -> Person {
        let newPerson = Person(context: moc)
        let df = DateFormatter()
        let bdate = df.date(from: "1988-12-11")
        newPerson.dateofbirth = bdate!
        newPerson.id = 103
        newPerson.name = "Bob"
        newPerson.lastname = "Smith"
        return newPerson
    }
}
